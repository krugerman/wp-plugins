#!/bin/bash

#sudo npm install --global wp-pot-cli

cd smtp-mailer-wp
wp-pot --dest-file 'languages/smtp-mailer-wp.pot' --domain 'smtp-mailer-wp'
cd ..

cd login-attempts-limit-wp
wp-pot --dest-file 'languages/login-attempts-limit-wp.pot' --domain 'login-attempts-limit-wp'
cd ..